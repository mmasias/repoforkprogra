public class unMundo{
	public static void main(String[] args){
		
		// Generador de mapas: https://bit.ly/mapaProgra1-2122
		int[][] a = {
			{2,0,0,0,0,0,0,0,0,0,3,3,3,3,3,3,1,0,0,0,0,0,0,0,2},
			{2,0,0,1,1,1,1,1,0,0,0,3,3,3,3,3,1,0,0,0,0,0,0,0,2},
			{2,2,0,1,0,0,1,0,0,0,0,0,0,0,3,3,1,0,0,0,0,0,0,0,0},
			{2,2,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,2},
			{2,2,2,0,0,0,1,0,0,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,2},
			{2,2,2,0,0,0,1,0,2,2,2,2,0,0,0,2,0,0,0,0,0,0,0,0,2},
			{2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,2,2,0,0,0,0,0,2,2},
			{2,2,2,0,0,0,3,3,3,2,2,0,0,0,0,0,0,0,2,0,0,0,0,2,2},
			{2,2,2,0,0,0,3,3,1,1,0,0,1,1,0,1,1,1,0,2,2,0,0,2,2},
			{2,2,2,0,3,3,3,3,1,1,0,0,1,1,0,1,2,1,0,0,0,2,2,2,2},
			{2,2,2,0,0,3,3,0,1,1,1,1,1,1,0,1,2,1,0,0,0,0,0,2,2},
			{2,2,2,0,0,3,3,0,0,1,0,0,1,0,0,1,2,1,0,0,0,0,0,0,2},
			{2,2,2,0,3,3,0,0,0,1,0,0,1,0,0,1,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,0,1,0,0,1,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,0,0,0,0,0,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,0,0,0,1,0,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,0,0,1,0,0,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,0,1,0,0,0,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,1,0,0,0,1,2,1,0,0,0,0,0,0,0},
			{2,2,2,0,0,0,0,0,0,1,0,1,0,0,0,1,1,1,0,0,0,0,0,0,1}
		};

		int cantidadFilas = a.length;
		int cantidadColumnas = a[0].length;

		System.out.print("+");for (int i=0;i<cantidadColumnas;i=i+1){System.out.print("---");};System.out.println("+");	// Borde superior
		for (int unaFila=0; unaFila < cantidadFilas; unaFila=unaFila+1){
			System.out.print("|");																						// Borde izquierdo
			for (int unaColumna=0; unaColumna < cantidadColumnas; unaColumna=unaColumna+1) {
				// Parseo del mapa
				if (a[unaFila][unaColumna]==0)	{	System.out.print(" . ");	} else 
				if (a[unaFila][unaColumna]==1) 	{	System.out.print("[#]");	} else 
				if (a[unaFila][unaColumna]==2)	{	System.out.print("~~~");	} else 
				if (a[unaFila][unaColumna]==3)	{	System.out.print("oOo");	}

			}
			System.out.println("|");																					// Borde derecho y salto de línea
		}
		System.out.print("+");for (int i=0;i<cantidadColumnas;i=i+1){System.out.print("---");};System.out.println("+");	// Borde inferior
	}
}